public class Persona{
  private String nombre;
  private String apellido;
  private int edad;
  private char sexo;

  

  public Persona(String nombre, String apellido, int edad, char sexo){
    this.nombre=nombre;
    this.apellido=apellido;
    this.edad=edad;
    this.sexo=sexo;
  }

  public Persona(String nombre, int edad, char sexo){
    this.nombre=nombre;
    this.edad=edad;
    this.sexo=sexo;
  }

  public void setNombre(String nombre){
    this.nombre=nombre;
  }

  public String getNombre(){
    return nombre;
  }

  public void setApellido(String apellido){
  	this.apellido=apellido;
  }

  public String getApellido(){
  	return apellido;
  }

  public void setEdad(int edad){
    this.edad=edad;
  }

  public int getEdad(){
    return edad;
  }

  public void setSexo(char sexo){
    this.sexo=sexo;
  }

  public char getSexo(){
    return sexo;
  }

  public String toString(){
    return "\nNombre: " + nombre +
           "\nEdad: " + edad +
           "\nSexo: " + sexo;
  }

}
