public class MergeSort{
	private int tamanio;
	private Persona[] personas;
	private long TInicio,TFin;
	private double tiempo;

    public static void mergeSort(Persona[] nombres) {
        if (nombres.length > 2) {
            Persona[] left = new Persona[nombres.length / 2];
            Persona[] right = new Persona[nombres.length - nombres.length / 2];

            for (int i = 0; i < left.length; i++) {
                left[i] = nombres[i];
            }

            for (int i = 0; i < right.length; i++) {
                right[i] = nombres[i + nombres.length / 2];
            }

            mergeSort(left);
            mergeSort(right);
            merge(nombres, left, right);
        }
    }

    public static void merge(Persona[] nombres, Persona[] left, Persona[] right) {
        int a = 0;
        int b = 0;
        for (int i = 0; i < nombres.length; i++) {
            if (b >= right.length || (a < left.length && left[a].getNombre().compareToIgnoreCase(right[b].getNombre()) < 0)) {
                nombres[i] = left[a];
                a++;
            } else {
                nombres[i] = right[b];
                b++;
            }
        }
    }




	public void imprimirOrdenado(Persona[] personas){
  		TInicio = System.nanoTime(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
   		mergeSort(personas);
    	TFin = System.nanoTime(); //Tomamos la hora en que finalizó el algoritmo y la almacenamos en la variable T
 		tiempo = (TFin - TInicio); //Calculamos los milisegundos de diferencia
 	 
 	 System.out.println("\nPersonas ordenados por nombre:(MergeSort) ");
  	
  		for(int j=0;j<personas.length;j++){
    	//	System.out.println(personas[j].toString());
 		 }
 	 	 System.out.println("\n\tTiempo de ejecucion: " + tiempo + " nanosegundos");
	 
	}	
}