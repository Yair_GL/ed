public class SelectionSort{
	private int tamanio;
	private Persona[] personas;
	private long TInicio,TFin;
	private double tiempo;

	public SelectionSort(){

	}

	public void selectionSort(Persona[] personas){
	for (int i = 0; i < personas.length - 1; i++) {
    	for (int j = i + 1; j < personas.length; j++) {
        	if (personas[i].getNombre().compareTo(personas[j].getNombre()) > 1) {
            	Persona temp = personas[i];
            	personas[i] = personas[j];
            	personas[j] = temp;
        		}
    		}
		}	
	}

	public void imprimirOrdenado(Persona[] personas){
  		TInicio = System.nanoTime(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
   		selectionSort(personas);
    	TFin = System.nanoTime(); //Tomamos la hora en que finalizó el algoritmo y la almacenamos en la variable T
 		tiempo = (TFin - TInicio); //Calculamos los milisegundos de diferencia
 	 
 	 System.out.println("\nPersonas ordenados por nombre:(SelectionSort) ");
  	
  		for(int j=0;j<personas.length;j++){
    	//	System.out.println(personas[j].toString());
 		 }
  	 System.out.println("\n\tTiempo de ejecucion: " + tiempo + " nanosegundos");

	}



}