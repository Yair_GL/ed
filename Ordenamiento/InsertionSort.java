public class InsertionSort{
	private int tamanio;
	private Persona[] personas;
	private long TInicio,TFin;
	private double tiempo;


	public InsertionSort(){

	}

  public static void insertionSort(Persona[] personas) {
    int i=0,j;
    for (j = 1; j < personas.length; j++) {
      Persona temp = personas[j];
      i = j;

      while (i > 0 && personas[i - 1].getNombre().compareTo(temp.getNombre()) > 0) {
        personas[i] = personas[i - 1];
        --i;
      }
      personas[i] = temp;
    }
  }




public void imprimirOrdenado(Persona[] personas){
  	TInicio = System.nanoTime(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
      insertionSort(personas);
    TFin = System.nanoTime(); //Tomamos la hora en que finalizó el algoritmo y la almacenamos en la variable T
 		tiempo = (TFin - TInicio); //Calculamos los milisegundos de diferencia

  
  System.out.println("\nPersonas ordenados por nombre:(InsertionSort) ");
  for(int j=0;j<personas.length;j++){
    //System.out.println(personas[j].toString());
 	 }
   System.out.println("\n\tTiempo de ejecucion: " + tiempo + " nanosegundos");
}


}