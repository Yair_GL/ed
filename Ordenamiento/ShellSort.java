public class ShellSort{
	private int tamanio;
	private Persona[] personas;
	private long TInicio,TFin;
	private double tiempo;

	public ShellSort(){

	}

	public void shellSort(Persona[] a){
    int j;
    for( int gap = a.length / 2; gap > 0; gap /= 2 ){
      for( int i = gap; i < a.length; i++ ){
         Persona tmp = a[ i ];
         for( j = i; j >= gap && tmp.getNombre().compareTo( a[ j - gap ].getNombre() ) < 0; j -= gap ){
           a[ j ] = a[ j - gap ];
         }
         a[ j ] = tmp;
      }
    }
  }

    	public void imprimirOrdenado(Persona[] personas){
  		TInicio = System.nanoTime();
   		shellSort(personas);
    	TFin = System.nanoTime(); 
 		tiempo = (TFin - TInicio); 
 		 System.out.println("\nPersonas ordenados por nombre:(ShellSort) ");
  	
  		for(int j=0;j<personas.length;j++){
    	//	System.out.println(personas[j].toString());
 		 }
  		 System.out.println("\n\tTiempo de ejecucion: " + tiempo + " nanosegundos");

	}	


}