import java.util.Random;
public class PruebaOrdenamiento{
	public static int tamanio;
	public static String[] nombres={"Adonis",	
"Adrastos",	
"Aegidius",	
"Aeneas",
"Aetos",
"Agatone",
"Ajax",
"Akiles",
"Alcander",
"Alekos",
"Alesandro",
"Alexander",
"Alexandros",
"Alexios",
"Andreus",
"Angelo",
"Anker",
"Apolo",
"Apostolos",
"Ares",
"Argo",
"Argus",
"Arion",
"Aristides",
"Aristo",
"Aristotle",
"Arsenios",
"Athan",
"Attis",
"Basil",
"Bastiaan",
"Bemus",
"Calisto",
"Calix",
"Castor",
"Christopher",
"Claus",
"Cletus",
"Constantine",
"Costa",
"Cyrano",
"Cyril",
"Damen",
"Damian",
"Damianos",
"Damon",
"Dareios",
"Deacon",
"Demetrius",
"Deo",
"Dinos",
"Dion",
"Dionysius",
"Dorian",
"Dymas",
"Erasmo",
"Erasmus",
"Eros",
"Eryx",
"Eudor",
"Eugene",
"Eusebio",
"Eusthathius",
"Evander",
"Evandros",
"Galenus",
"George",
"Gregor",
"Hercules",
"Hippolytus",
"Homer",
"Iakovos",
"Icarus",
"Ioannes",
"Isidore",
"Isidore",
"Janus",
"Jerry",
"Julio",
"Karsten",
"Khristos",
"Kit",
"Kosmos",
"Laertes",
"Lander",
"Lazarus",
"Leander",
"Linus",
"Lukas",
"Lysander",
"Marinos",
"Mateo",
"Melancton",
"Moris",
"Myles",
"Myron",
"Napoleon",
"Narkissos",
"Nemo",
"Neo",
"Nestor",
"Nicholas",
"Nicodemus",
"Niles",
"Odysseus",
"Otis",
"Panos",
"Panthea",
"Paris",
"Pedro",
"Pericles",
"Philip",
"Phoenix",
"Proteus",
"Rhodes",
"Semon",
"Sirius",
"Sofronio",
"Soterios",
"Spiro",
"Stavros",
"Stephanos",
"Talos",
"Tarasios",
"Tassos",
"Thanos",
"Theo",
"Theodore",
"Theron",
"Thomas",
"Timoleon",
"Timon",
"Titan",
"Titus",
"Tyrone",
"Urian",
"Vasili",
"Vasilios",
"Vitalis",
"Xander",
"Xylon",
"Yanni",
"Zale",
"Zander",
"Zarek",
"Zenobio",
"Zoello",
"Zoilo",
"Zosimo",
"Zotico"};
public static String[] apellidos={"Garcia",
"Ramos",
"Torres",
"Hernandez", 
"Ñiguez",
"Almira", 
"Rodríguez",
"Aldeguer",
"Andreu", 
"Serrano",
"Chazarra",
"Ortiz",
"Macia",
"Alonso",
"Espuch",
"Alarcon",
"Parada",
"Amores",
"Vidal", 
"Arevalo",
"Osterreich",
"Fructuoso",
"Monteliu",
"Montesinos",
"Cecilia",
"Imbernon", 
"Lorente",
"Conchita",
"Cristian",
"de la fuent",
"Sanchez",
"Herrera",
"Albaladejo",
"Torregrosa", 
"Manzanaro",
"Albaladejo",
"Argueso",
"Auñon",
"Maldonado",
"Garres",
"Peregrin",
"Encarnita",
"Andreu", 
"Martinez",
"Diaz", 
"Arenas",
"Fernandez", 
"Mendez",
"Felipe",
"Grau",
"Albaladejo",
"Gutierrez",
"Torres",
"Fulgencio",
"Almira",
"Albaladejo",
"Guzman",
"Ballesta",
"Helena",
"Griñan",
"Tejuelo", 
"Tormes",
"Jung",
"Andreu",
"Ivan",
"Sala",
"Jaqueline",
"Guillamon",
"Paredes",
"Jens",
"Blaum",
"Joan",
"Joaquin",
"Saez", 
"Fulgencio",
"Perez", 
"Vidal",
"Jose",
"Antonio",
"Miñano",
"Minguez",
"Suarez",
"Barredo",
"Juan",
"Tomas",
"Penades", 
"Cambra",
"Macia",
"Miguel",
"Aniorte", 
"Canovas",
"Ruiz",
"Espinosa",
"Vicente", 
"Canales",
"Sanchez",
"Albaladejo", 
"Alfonso", 
"Villaplana",
"Bellot",
"Torres",
"Torregrosa",
"Fernandez",
"Galant",
"Cerdan", 
"Leocadia",
"Angeles", 
"Pizana", 
"Antonia",
"del Mar", 
"Carrasco",
"Galvez",
"Dolores",
"Coral", 
"Sanchez",
"Isabel", 
"Rivera",
"Hernandez",
"Quiles",
"Fernandez", 
"Cayuelas",
"Quesada",
"Zambrana",
"Luis", 
"Barraquel", 
"Navarro",
"Jimenez",
"Pilar",
"Jurado",
"Ferrandez",
"Montesinos",
"Carbonel",
"Vera",
"Quesada",
"Mazon",
"Mateo",
"Lozano", 
"Pulido",
"Palomo", 
"Dlugi",
"Nieves", 
"Bautista",
"Torres",
"Puigcerver",
"Costa",
"Rosas",
"Ortiz",
"Gomez",
"Gasser", 
"Delgado"};
		

	public static String[] nombresAleatorios(int tamanio){
		String[] nombresAleatorios = new String[tamanio];
		for (int i = 0; i < tamanio; i++) {
		nombresAleatorios[i]= nombres[(int)(Math.floor(Math.random()*((nombres.length-1)-0+1)+0))]+" "+ apellidos[(int)(Math.floor(Math.random()*((apellidos.length-1)-0+1)+0))];
	}
	return nombresAleatorios;
}
	

	public static void main(String[] args){
	Persona[] personas10 = new Persona[10];
	Persona[] personas100 = new Persona[100];
	Persona[] personas1000 = new Persona[1000];
	Persona[] personas10000 = new Persona[10000];
	Persona[] personas100000 = new Persona[100000];
	Persona[] personas1000000 = new Persona[1000000];

	
		/*
		for(int i=0;i<150;i++){
			personas[i] = new Persona(nombres[i],apellidos[i],i,'H');
		}

		for(int i=0;i<150;i++){
			System.out.println(personas[i].toString());
		} */

		//generamos 10 


		String[] aux = new String[100000];
		aux = nombresAleatorios(100000);
		
		for(int i=0;i<100000;i++){
			personas100000[i] = new Persona(aux[i],i,'H');
		}


		System.out.println("\n**************************************************************************\n");
		System.out.println("\t\t\t ARREGLO 10 PERSONAS\n\n");
	/*	System.out.println("Arreglo desordenado");
		for(int i=0;i<10;i++){
			System.out.println(personas10[i].toString());
		} */

		BubbleSort burbuja = new BubbleSort();
		System.out.println("Arreglo ordenado");
	//	burbuja.imprimirOrdenado(personas100000);

		QuickSort quick = new QuickSort();
		System.out.println("Arreglo ordenado");
		quick.imprimirOrdenado(personas100000);

		InsertionSort iS = new InsertionSort();
		System.out.println("Arreglo ordenado");
		iS.imprimirOrdenado(personas100000);

		MergeSort merge = new MergeSort();
		System.out.println("Arreglo ordenado");		
		merge.imprimirOrdenado(personas100000);

		ShellSort shell = new ShellSort();
		System.out.println("Arreglo ordenado");		
		shell.imprimirOrdenado(personas100000);

		SelectionSort sSort = new SelectionSort();
		System.out.println("Arreglo ordenado");		
		sSort.imprimirOrdenado(personas100000);	
		
		HeapSort hSort = new HeapSort();
		System.out.println("Arreglo ordenado");
		hSort.imprimirOrdenado(personas100000);


		System.out.println("\n**************************************************************************\n");

		String[] auxMillon = new String[1000000];
		auxMillon = nombresAleatorios(1000000);
		
		for(int i=0;i<1000000;i++){
			personas1000000[i] = new Persona(auxMillon[i],i,'H');
		}

		/*
		System.out.println("Arreglo desordenado");
		for(int i=0;i<1000000;i++){
			System.out.println(personas1000000[i].toString());
		} 
		
		BubbleSort burbujaMillon = new BubbleSort(); //slow
		System.out.println("Arreglo ordenado");
		burbujaMillon.imprimirOrdenado(personas1000000);
	
		QuickSort quickMillon = new QuickSort();
		System.out.println("Arreglo ordenado");
		quickMillon.imprimirOrdenado(personas1000000);
       
		MergeSort mergeMillon = new MergeSort();
		System.out.println("Arreglo ordenado");
		mergeMillon.imprimirOrdenado(personas1000000);		

		ShellSort shellMillon = new ShellSort();
		System.out.println("Arreglo ordenado");
		shellMillon.imprimirOrdenado(personas1000000);			

		HeapSort monticulo = new HeapSort();
		System.out.println("Arreglo ordenado");
		monticulo.imprimirOrdenado(personas1000000);	

		InsertionSort iSortMillon = new InsertionSort(); //slow
		System.out.println("Arreglo ordenado");
		iSortMillon.imprimirOrdenado(personas1000000);				
	
		SelectionSort sSortMillon = new SelectionSort();
		System.out.println("Arreglo ordenado");
		sSortMillon.imprimirOrdenado(personas1000000);			
	*/


		

	}
}