//monticulo

public class HeapSort{
	private int tamanio;
	private Persona[] personas;
	private long TInicio,TFin;
	private double tiempo;

	public HeapSort(){

	}

	public static void sort(Persona[] pq) {
        int n = pq.length;
        for (int k = n/2; k >= 1; k--)
            sink(pq, k, n);
        while (n > 1) {
            exch(pq, 1, n--);
            sink(pq, 1, n);
        }
    }

    private static void sink(Persona[] pq, int k, int n) {
        while (2*k <= n) {
            int j = 2*k;
            if (j < n && less(pq, j, j+1)) j++;
            if (!less(pq, k, j)) break;
            exch(pq, k, j);
            k = j;
        }
    }

  
    private static boolean less(Persona[] pq, int i, int j) {
        return pq[i-1].getNombre().compareTo(pq[j-1].getNombre()) < 0;
    }

    private static void exch(Object[] pq, int i, int j) {
        Object swap = pq[i-1];
        pq[i-1] = pq[j-1];
        pq[j-1] = swap;
    }


  	public void imprimirOrdenado(Persona[] personas){
  		TInicio = System.nanoTime(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
   		sort(personas);
    	TFin = System.nanoTime(); //Tomamos la hora en que finalizó el algoritmo y la almacenamos en la variable T
 		tiempo = (TFin - TInicio); //Calculamos los milisegundos de diferencia
 	 
 	 System.out.println("\nPersonas ordenados por nombre:(HeapSort(Monticulo)) ");
  	
  		for(int j=0;j<personas.length;j++){
    	//	System.out.println(personas[j].toString());
 		 }
 		 System.out.println("\n\tTiempo de ejecucion: " + tiempo + " nanosegundos");

	}	




}