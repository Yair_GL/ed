public class QuickSort{
	private int tamanio;
	private Persona[] personas;
	private long TInicio,TFin;
	private double tiempo;

	public QuickSort(){

	}

       public static void quicksort(Persona[] a, int lo, int hi) {
            if(lo >= hi) return;
            int pi = particion(a, lo, hi);
            quicksort(a, lo, pi-1);
            quicksort(a, pi+1, hi);
        }

        public static int particion(Persona[] a, int lo, int hi) {
            int i = lo + 1;
            int j = hi;

            while(i <= j) {
                if(a[i].getNombre().compareTo(a[lo].getNombre()) <= 0) { 
                    i++; 
                }
                else if(a[j].getNombre().compareTo(a[lo].getNombre()) > 0) { 
                    j--;
                }
                else if(j < i) {
                    break;
                }
                else
                    swap(a, i, j);
            }
            swap(a, lo, j);
            return j;
        }

        private static void swap(Persona[] a, int i, int j) {
            Persona tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
        }



public void imprimirOrdenado(Persona[] personas){

  	TInicio = System.nanoTime(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
      quicksort(personas,0,personas.length-1);
    TFin = System.nanoTime(); //Tomamos la hora en que finalizó el algoritmo y la almacenamos en la variable T
 		tiempo = (TFin - TInicio); //Calculamos los milisegundos de diferencia

  
  System.out.println("\nPersonas ordenados por nombre (QuickSort): ");
  System.out.println("\n\tTiempo de ejecucion: " + tiempo + " nanosegundos");
  for(int j=0;j<personas.length;j++){
   // System.out.println(personas[j].toString());
  }
	}

}
